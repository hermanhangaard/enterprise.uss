# Main report

[[_TOC_]]
***
## 1.0 Introduksjon:
Tankegangen vår for prosjektet var å automatisere så mye av koden som mulig. Vi oppnådde dette, så langt det lot seg gjøre, ved å sentralisere kjøringen av koden vår i “dc-main.ps1”. Hovedscriptet fungerer ved å kjøre disse ekstra scriptene etter behov. 

Mye av kodene vi bruker er blitt tatt fra [kompendiet](https://gitlab.com/erikhje/dcsg1005/-/blob/master/compendia.md) til Erik Hjelmås. Hvis det ikke blir henvist til kilder, så kommer informasjonen enten fra hyperlinken, fra [powershell](https://gitlab.com/erikhje/dcsg1005/-/blob/master/powershell.md) delen av kompendiet. Eller fra vår egen forståelse og kompetanse.
***
### 1.1 ScriptsAD.zip
ScriptsAD.zip vil inneholde alle scriptene dc-main tar i bruk under runtime. Vi valgte å gå for denne metoden ettersom vi ikke ville ha et stort uleselig hovedprogram, og for å unngå nettverks-avhengigheter og andre nettverksrelaterte problemer under runtime. Selve zip filen vil bli lastet ned ved hjelp av curl og unzipped med “Expand-Archive”.

>**ScriptsAD.zip:**
>- dc-init.ps1
>- dc-main.ps1
>- usr-setup-csv.ps1
>- gpo-init.ps1
>- grp-init.ps1
>- GPO (directory)
***
## 2.0 Scripts:

### 2.1 dc-init.ps1
Dc-init scriptet setter grunnlaget for mange av de andre scriptene som skal kjørers senere i programmet.

#### Funksjonalitet
1. Scriptet setter [språk, tidssone og tidsformat](https://stackoverflow.com/questions/28749439/changing-windows-time-and-date-format).
2. Laster ned Chocolatey som igjen laster ned powershell-core, sysinternals og 7-zip. 
3. Laster ned en bgi config fil, legger den på skrivebordet, kjører programmet med config filen og setter config filen til startup. 
4. Installerer featurene ADDS og DNS, lager “Administrator” bruker. Sjekker for gyldig passord. Hasher så options for forest, installerer og restarter maskinen.

#### Problemer
**Invoke-Expression**

- Vi hadde problemer med [scriptet](https://chocolatey.org/install) Chocolatey lagde for powershell. Den hadde “Invoke-Expression” noe som bør unngås i script. Dette ble byttet med “Invoke-WebRequest” som fungerte fint. 
***
### 2.2 srv-init-join.ps1
Srv-init-join scriptet installerer også initielle parametere og verktøy. Scriptet gjør i tillegg srv1 tilgjengelig via RDP.

#### Funksjonalitet
1. Scriptet har samme funksjonalitet som punkt 1, 2 og 3 i dc-init.ps1
2. Åpner for RDP

#### Problemer 
- Ingen problemer. Mesteparten av koden er lik dc-init.ps1.
***
### 2.3 cl-mgr-init.ps1
Dette scriptet installerer initielle parametere og åpner opp for PSRemoting.

#### Funksjonalitet
1. Scriptet setter [språk, tidssone og tidsformat](https://stackoverflow.com/questions/28749439/changing-windows-time-and-date-format).
2. Åpner for RDP

#### Problemer
**PSRemoting**

- Vi møtte på problemer når vi kjørte koder for PSRemoting fra kompendiet, og måtte finne en annen metode for å [åpne for RDP](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/enable-psremoting?view=powershell-7.2). Vi har indikatorer for at denne cmdlet’en ikke er helt sikker, men vi fant ingen annen god løsning på problemet. 
***
### 2.4 usr-setup-csv.ps1
Usr-setup scriptet danner 100 brukere med randomiserte navn og passord.

#### Funksjonalitet
1. Sjekker om brukere allerede er blitt lagd eller ikke
2. Lager 100 brukere
3. [Danner et sterkt 16 tegns passord](https://www.powershellbros.com/generating-password-using-powershell/)
4. Laster opp brukerne i enterpriseussusers.csv

#### Problemer

**Random passord generering** 

- Vi ville ikke lage et 16 tegns tilfeldig passord med -join koden fra kompendiet. Derfor måtte vi finne en annen måte å gjøre dette på. Dette gjorde vi med bruk av en [funksjon.](https://www.powershellbros.com/generating-password-using-powershell/)
***
### 2.5 gpo-init.ps1
Tanken rundt gpo-init var å automatisere dannelsen og implementeringen av allerede nedlastede GPO-er. 

| Navn | Beskrivelse | OU |
| ---- | ----------- | -- |
| Default Domain Policy | Mtp. innlogging hadde vi fokus på et sterkere passord med hardere straffer. **Settings:** Max pass age 90, 12 char password, login attempts 6, lockout 30min | Enterprise.uss |
| User Restrictions | Prøver å begrense brukeren sitt "handlingsrom". **Settings:** Hide C:\, disable powershell - cmd - regedit, restrict control panel, disable removable media, force wallpaper | AllUsers |
| Admin Privileges | **Settings:** negerer alt i "User Restrictions" | Adm | 
| Machine Audit | Få informasjon om hva som skjer i domenet. **Settings:** audit account logon events, audit account management, audit logon events, audit privilage use | Machines | 
| Computer Restrictions | Generel sikkerhet som kjøres på alle maskiner. **Settings:** force sleep, disable windows insaller, disable guest account | Clients |
| MSFT Windows Server 2022 - Domain Controller | Bruker alle baselinene fra microsoft og legger en egen setting. **Settings:** disable forced restarts | Domain Controllers |
| MSFT Windows Server 2022 - Member Server | Bruker alle baselinene fra microsoft og legger en egen setting.  **Settings:** disable forced restarts | Member Servers |


#### Funksjonalitet
1. Fjerner GPO-link fra allerede installert “Default Domain Policy” og renamer den “Old Default Domain Policy”
2. Lager nye GPO-er og importerer dem. GPO navn og ID blir funnet via en loop som går gjennom xml filen til alle GPO-ene vi har lastet ned.
3. Linker de bestemte GPO-ene til sin utvalgte OU.

#### Problemer
**Få ut objektene GPODisplayName og ID fra xml fil**

- Når vi skulle få tak i GPODisplayName og ID fra xml filen hadde vi vansker med å få ut bare objekt stringen. For å fikse dette måtte vi finne ut hvilket format de ble skrevet ut i, noe som var “#cdata-section”.
***
### 2.6 grp-init.ps1
Grp-init tar seg av oppretting av grupper i følge AGDLP og medlemmene disse gruppene skal ha. Den lager i tillegg SMB share og gir tilgang til denne. 

#### Funksjonalitet

1. Hasher og lager Global og Domain Local grupper

#### Problemer
**Legge til flere brukere i gruppe samtidig**

- Vi hadde vansker med å flytte en bestemt gruppe brukere inn i en group. For å fikse dette brukte vi en [foreach loop](https://stackoverflow.com/questions/17683368/running-a-command-on-each-directory-in-a-list-using-powershell).
***
### 2.7 dc-main.ps1

Dc-main scriptet vårt bruker vi for alle hovedfunksjonene vi vil utføre på domenekontrolleren. Dette er det desiderte største scriptet vårt.

#### Funksjonalitet

1. Oppretter OU’er, GPO’er, grupper og brukere, med en duplikatsjekk.
2. Spør administrator om maskiner ønskes opprettet ved innlesning, helt til admin taster Q(uit). Om admin taster K(lienter), M(anager) eller S(erver) vil scriptet spørre om nødvendig informasjon om maskinen og legge den i Enterprise.USS-domene med riktig OU-path. Deretter vil den restarte maskinen slik at endringene blir påført. PS-Session vil også fjernes.  

#### Problemer

**Struktur**

- Vi var usikre på hvordan dc-main skulle være strukturert. Vår første tanke var at koden kunne ligne litt på programmer vi har lagd i programmeringsfagene. Vi undersøkte derfor hvordan powershell opererer med løkker og så at det var omtrent helt identisk. Derfra begynte vi å skrive strukturen fra bunnen av.   

**New-PSSession**

- Vi ønsket å gjøre mest mulig fra domenekontroller. Ved litt undersøking på nettet fant vi commandlet’en [New-PSSession](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/new-pssession?view=powershell-7.2) som hjalp oss med å skape en link med de andre maskinene og utføre remote cmdlets. Dette var ikke lett å få til og automatisere i scriptet med variabler av nødvendige ressurser, men etter mange timer med testing fikk vi det endelig til.

**Remote variables**

- Ved bruk av New-PSSession ønsket vi å utføre cmdlets på maskinen vi koblet til. Blant disse krevdes IP-adressen til domenekontrolleren for å sette DNS. Etter både litt søking og testing kom vi fram til “scope modifier”en [$Using:](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_remote_variables?view=powershell-7.2).

**Try, Catch og Finally**

- Siden vi opprettet en veldig lang main med mange muligheter for feil og duplikater av det som skjer automatisk, ønsket vi en cmdlet som prøvde noe og avbrøt om en feil skulle oppstå. Fra dette utganspunktet fant vi fram til [Try, Catch, Finally](https://docs.microsoft.com/en-us/powershell/module/microsoft.powershell.core/about/about_try_catch_finally?view=powershell-7.2) funksjonen. Dette har vi brukt i på main-scriptet vårt for å sjekke om ressurser allerede eksisterer. Fordelen ved dette er at vi kan kjøre main-scriptet hver gang vi trenger en ny maskin.
***
## 3.0 Oppsummering:
Vi har lagd et hovedscript som initierer våre subscripts på en effektiv og uproblematisk måte. Scriptene er blitt kjørt og feilsjekket flere ganger. Ved arbeidets ende kjører scriptene feilfritt. Vi ville gjøre AD oppsett så enkelt og effektivt som mulig, som vi føler vi har gjort på en tilfredsstillende måte. 

Vi har fortsatt en god del funksjonalitet som vi ikke fikk mulighet til å implementere, men er fornøyd med innsatsen fra alle i teamet og føler vi har levert et godt produkt.




