# Plan

Vi skal danne en Active Directory for bedriften “USS Enterprise”. USS Enterprise er et lite til middels stort firma, som inneholder en personalavdeling, konsulentavdeling og administrativ avdeling. Under konsulentavdelingen har vi tre underavdelinger som har hver sine fordelinger av  GPO’er. Disse GPO’ene utdeler nødvendige rettigheter til hva avdelingene trenger. Vårt ferdigstilte prosjekt viser hvordan vi setter opp Active Directory, hvilke scripts vi har dannet for at det skal bli mest mulig effektivt og hvordan AD-strukturen blir.  

Når vi tenker på det tekniske i dette prosjektet har vi stort fokus på git. Vi ønsker et ryddig system med “ADD”-melding for nye filer, “MOD”-melding for endring av allerede eksisterende filer og “RM”-meldinger for sletting av filer. Disse vil oppfølges av en enkel kommentar. 
Vi skal også skrive en rapport på omkring 1200 ord som vil i detalj beskrive vår tankegang og progresjon gjennom prosjektet.
Når det gjelder oppsett av en AD ønsker vi å effektivisere alt gjennom scripts. Vi ønsker et lite antall store scripts. Med denne tankegangen er det derimot viktig at scriptene gjør sin oppgave på en ryddig måte og ikke blir “for store”, samt brukervennlige. Vi vil benytte oss av psscriptsanalyzer for å sørge for at scriptene er skrevet iht “best practice”.

Til slutt vil vi danne en video med en brukervennlig gjennomgang av AD-opprettelsen og hvordan sluttproduktet vil se ut. Vårt mål er at alle som ser denne videoen og har tilgang til vår git, skal kunne danne denne infrastrukturen med lite til ingen hjelp og gjøre dette ved bruk av få steg. 

# Roller

**Prosjektleder** - Markus Olsvik

- Prosjektlederen er ansvarlig for å dele ut oppgaver, løse konflikter og motivere teamet innsatsen om den skulle synke til et uakseptabelt nivå.

**Møteansvarlig** - Mats Staveland

- Møteansvarlig kaller inn til møter, skriver møtereferater og finner tid/sted for nevnte møter.

**Kvalitetssikrer** - Herman Hangaard

- Kvalitetssikreren er siste ledd før innlevering. Går over ferdigstilte dokumenter og sjekker kvalitetsfaktorer som grammatikk og struktur.

# Møtereferater

### Uke 1:

**Mandag 28.02.2022 - 2 timer**

- Vi forka git-templaten til Erik og dannet en prosjektplan. Deretter plana vi målene våre for tirsdag 1.mars som inkluderte en lang økt for å få oversikt over prosjektet og komme foran arbeidet vi har foran oss. Vi startet også smått med å se på scripts og AD. 

**Tirsdag 01.03.2022 - 4 timer**

- Vi har eksperimetert med scripts, i tillegg til å ha satt opp og lastet ned AD på hver maskin. Vi har hovedsakelig forsket på scripts som skal legge til hosts i AD. Vi har også rukket å danne små scripts som vi tenker å komprimere til ett script, som da er til spesifikke maskiner. I tillegg har vi øvd oss på git commits slik at dette er på plass, samt commit kommentarer og forkortelser som "MOD", "ADD" og "RM". Neste gang håper vi på å starte med main-scriptet vårt. 

**Onsdag 02.03.2022 - 3 timer**

- Vi har jobbet videre med oppsett av scripts. Vi har startet med dc-main scriptet vårt. Vi har også lest oss opp på hvordan man får tak i git scripts selvom gitlab er privat. I tillegg til dette har vi funnet ut av hvordan vi skal klare å bruke Enter-PSSession til å kjøre scripts på de andre maskinene via dc. Til neste gang håper vi å jobbe videre med dette.

**Torsdag 03.03.2022 - 5 timer**

- Idag har vi jobbet videre med New-PSSession for å bli med i domene med de andre maskinene fra dc1. Dette lyktes vi med og satser på å ferdigstille dette neste gang. Vi titta også nærmere på private gitlab-linker, men innså at dette ble ekstremt vanskelig og kasta bort mange timer på dette. I tillegg skrev vi kode for oppretting av OU'er og brukere. Planen for neste økt vil være å ferdigstille PSSession-koden, som nevnt tidligere, samt starte på GPO'er og grupper.
***
### Uke 2:

**Tirsdag 08.03.2022 - 5 timer**

- Dagen idag brukte vi på å undersøke GPO'er og grupper. Vi ferdigstilte også main-scriptet vårt og adderte noen flere kodelinjer med b.l.a. "Try", "Catch" og "Finally" sånn at kodestrukturen ble en god del annerledes. Til neste gang håper vi å jobbe mye med grupper og GPO'er, og begynne å rette perspektive vårt mot å bli ferdig. Muligens også starte på "main report".

**Torsdag 10.03.2022 - 5 timer**

- I dag har vi fikset URL for GPO'er som vi vil bruke som baseline. Vi har også startet å lage egne GPO'er. Vi fant tidlig ut at vi ville ha et sterkere passord og måtte derfor også endre dette i scriptet vårt. Ellers har vi satt opp path for GPO'er, og finjustert på noe kode. Det neste vi planlegger å gjøre er å implementere en måte for å laste ned våre egne ferdiglagde GPO'er og starte på "main report".
***
### Uke 3:

**Mandag 14.03.2022 - 4 timer**

- I dag har vi satt opp script for å kunne laste ned ferdiglagde GPO'er som vi selv har lagd, i tillegg til noen microsoft baselines der vi har endret eller lagt til ekstra policies. Vi har også lagd et script som importerer GPO-filene som er blitt lastet ned og deretter linker dem til sin bestemte OU. Til slutt forsøkte vi å sette opp en bakgrunn hos en klient via GPO. Til neste gang håper vi å ferdigstille alt av kode og endelig starte på "main report".

**Tirsdag 15.03.2022 - 6 timer**

- Dagen idag brukte vi på å ferdigstille all kode og legge det i en zip. Vi startet også på å utforme og skrive "main report" så vi er i rute til innlevering i slutten av uken. I morgen håper vi på å bli ferdig med "main report" og alt annet som skal gjenstå, så vi kan begynne på video til torsdag.

**Onsdag 16.03.2022 - 6 timer**

- Vi brukte dagens økt på å kjøre en generalprøve av hvordan vi vil at koden skal være strukturert i videoen. Vi kom også godt i gang "main report" og planla hvordan vi skulle kommentere under innspilling. I morgen satser vi på å spille inn, kvalitetssikre og gjøre oss klare for innlevering.

**Torsdag 17.03.2022 - 14 timer**

- Prosjektets siste dag har vi brukt på å skrive ferdig main report, addert plan og roller til minutes, samt spilt inn video. Deretter redigerte vi video og delegerte noen små oppgaver inn mot leveringsfristen.
***
#### Totalt: 54 timer
